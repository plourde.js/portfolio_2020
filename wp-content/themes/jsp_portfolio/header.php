<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package JSP_Portfolio
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'jsp_portfolio' ); ?></a>

	<header id="header" class="header-tops">
		<div class="container">
			<h1><a href="<?php home_url();?>">Jean-Sébastien Plourde</a> </h1>
			<h2>Un<span> graphiste passionné</span> de Québec</h2>
			

			<nav id="site-navigation" class="main-navigation nav-menu d-none d-lg-block">
			
				
			<!-- <button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'jean-sebatien-graphiste' ); ?></button> -->
			<?php
			wp_nav_menu(
				array(
					'theme_location' => 'menu-1',
					'menu_id'        => 'primary-menu',
				)
			);
			?>

			
			</nav><!-- #site-navigation -->
				
				
		
		<div class="social-links">
        <a href="#" class="twitter"><i class="icofont-twitter"></i></a>
      	</div>

		
		
	</header><!-- #masthead -->

