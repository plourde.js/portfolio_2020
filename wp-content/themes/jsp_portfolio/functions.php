<?php
/**
 * JSP Portfolio functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package JSP_Portfolio
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'jsp_portfolio_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function jsp_portfolio_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on JSP Portfolio, use a find and replace
		 * to change 'jsp_portfolio' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'jsp_portfolio', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-1' => esc_html__( 'Primary', 'jsp_portfolio' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'jsp_portfolio_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'jsp_portfolio_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function jsp_portfolio_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'jsp_portfolio_content_width', 640 );
}
add_action( 'after_setup_theme', 'jsp_portfolio_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function jsp_portfolio_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'jsp_portfolio' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'jsp_portfolio' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'jsp_portfolio_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function jsp_portfolio_scripts() {
	
	/*-------------------The Normalize--------------------------------*/
	wp_enqueue_style( 'jsp_portfolio-style', get_stylesheet_uri(), array(), _S_VERSION );
	wp_style_add_data( 'jsp_portfolio-style', 'rtl', 'replace' );

	
  
	/*-- Google Fonts --*/
	wp_enqueue_style( 'jsp_fonts','https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i', array(), '1');
	
  
	/*--  Vendor CSS Files --*/

	wp_enqueue_style( 'jsp_bootstrap', get_template_directory_uri() .'/vendor/bootstrap/css/bootstrap.min.css', array(), '1');
	wp_enqueue_style( 'jsp_icofont', get_template_directory_uri() .'/vendor/icofont/icofont.min.css', array(), '1');
	wp_enqueue_style( 'jsp_remixicon', get_template_directory_uri() .'/vendor/remixicon/remixicon.css', array(), '1');
	wp_enqueue_style( 'jsp_owl-carousel', get_template_directory_uri() .'/vendor/owl.carousel/assets/owl.carousel.min.css', array(), '1');
	wp_enqueue_style( 'jsp_boxicons', get_template_directory_uri() .'/vendor/boxicons/css/boxicons.min.css', array(), '1');
	wp_enqueue_style( 'jsp_venobox', get_template_directory_uri() .'/vendor/venobox/venobox.css', array(), '1');
	

	/*-------------------Main CSS--------------------------------*/
	wp_enqueue_style( 'jsp_main_style', get_template_directory_uri() .'/css/style.css', array(), '2');


	
	/*-------------------Tha Scripts----------------------------------*/
	wp_enqueue_script( 'jsp_portfolio-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true );
	

	/*------------------------------------scripts vendor-------------*/
	wp_enqueue_script( 'jsp_jquery.min', get_template_directory_uri() . '/vendor/jquery/jquery.min.js', array(), '1', true );
	wp_enqueue_script( 'jsp_bootstrap.bundle', get_template_directory_uri() . '/vendor/bootstrap/js/bootstrap.bundle.min.js', array(), '1', true );
	wp_enqueue_script( 'jsp_jquery.easing', get_template_directory_uri() . '/vendor/jquery.easing/jquery.easing.min.js', array(), '1', true );
	wp_enqueue_script( 'jsp_jquery.validate', get_template_directory_uri() . '/vendor/php-email-form/validate.js', array(), '1', true );
	wp_enqueue_script( 'jsp_jquery.waypoints', get_template_directory_uri() . '/vendor/waypoints/jquery.waypoints.min.js', array(), '1', true );
	wp_enqueue_script( 'jsp_jquery.counterup', get_template_directory_uri() . '/vendor/counterup/counterup.min.js', array(), '1', true );
	wp_enqueue_script( 'jsp_owl.carousel', get_template_directory_uri() . '/vendor/owl.carousel/owl.carousel.min.js', array(), '1', true ); 
	wp_enqueue_script( 'jsp_isotope', get_template_directory_uri() . '/vendor/isotope-layout/isotope.pkgd.min.js', array(), '1', true ); 
	wp_enqueue_script( 'jsp_venobox', get_template_directory_uri() . '/vendor/venobox/venobox.min.js', array(), '1', true );

	/*-------------------Tha Scripts main----------------------------------*/
	wp_enqueue_script( 'jsp_main', get_template_directory_uri() . '/js/main.js', array(), '1', true );


	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'jsp_portfolio_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

