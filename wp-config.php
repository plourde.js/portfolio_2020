<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'portfolio_2020' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'b6fv 4~TfC5So#hTZu#S$GgjmGcQ$BmUPWxmvyt&ik4x>UJ:E3`q0B/c2_?1=oKN' );
define( 'SECURE_AUTH_KEY',  '4rW:1I9^G_[WW9]xg6A+p,qHzio46<|+)uqzK!lm9CeS:%60&UR;_}b^~Gr7d[LH' );
define( 'LOGGED_IN_KEY',    'y>+Ub1FN0&tjq*K2+xN^T5{o&(,sG&[{i7+Y`O#4/$(Q72k$5T7UWVpm]L R9)3v' );
define( 'NONCE_KEY',        'kWQ=?g`2xx5vC1sn7z+5wt3g=cA?#qm=,{l&w+zVJc}61YL%YT VNj55uW{y_nY7' );
define( 'AUTH_SALT',        '_oH+{xX$PBF1z+L=ZC m}cisWW+hG_G&ao[>8KfN2r~d5rhhy4v^U>;`(/OtwpD?' );
define( 'SECURE_AUTH_SALT', '39F.i=eHmFrzUu*+t9!Wi~xpd>49N?ga}8xu EbsUX*+dMk0c%};Z!zGar<rlT<j' );
define( 'LOGGED_IN_SALT',   'F,]=ueeeILQV^+5b,ZIH!q]!3wV?t)_XZrg=wK9>0Lvdf4Z~.p|!q6ZOXq~O#BH_' );
define( 'NONCE_SALT',       '9K,?ncV]D3BcgZ4m*L)c,Osnf^KzyFjfm54U*Z-ZMXV[;XJ_;,v4tY]=9#A9-oJf' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_js_folio';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
